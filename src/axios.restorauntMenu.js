import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://lesson-68.firebaseio.com'
});

export default instance;