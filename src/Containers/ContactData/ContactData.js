import React, {Component} from 'react';
import './ContactData.css';
import Button from "../../Components/UI/Button/Button";
import axios from '../../axios.restorauntMenu';
import Spinner from "../../Components/UI/Spinner/Spinner";

class ContactData extends Component {

  state = {
    name: '',
    phone: '',
    street: '',
    loading: false,
  };

  inputChange = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  orderHandler = event => {
    event.preventDefault();
    if (this.state.name.length && this.state.phone.length && this.state.street.length) {
      this.setState({loading: true}, () => {
        let selectedDishes = Object.keys(this.props.dishes)
          .map(ingName => {
            return {[ingName]: this.props.dishes[ingName].amount};
          });
        const order = {
          dishes: selectedDishes,
          price: this.props.price,
          customer: {
            name: this.state.name,
            phone: this.state.phone,
            street: this.state.street,
          }
        };
        axios.post('/order.json', order).finally(() => {
          this.setState({loading: false});
        })
      });
    }
  };

  render() {
    let form = (
      <form>
        <input className="Input" value={this.state.name}
               onChange={this.inputChange} type="text" name="name"
               placeholder="Your Name"
        />
        <input className="Input" value={this.state.phone}
               onChange={this.inputChange} type="text" name="phone"
               placeholder="Your phone"
        />
        <input className="Input" value={this.state.street}
               onChange={this.inputChange} type="text" name="street"
               placeholder="Street"
        />
        <Button clicked={this.orderHandler}>ORDER</Button>
      </form>
    );
    if (this.state.loading) form = <Spinner/>;

    return (
      <div className='ContactData'>
        <h4>Enter your Contact Data</h4>
        {form}
      </div>
    );
  };
}

export default ContactData;