import React, {Component} from 'react';
import DishList from "../../Components/Menu/DishList/DishList";
import Cart from "../../Components/Cart/Cart";

class RestaurantMenu extends Component {
  render() {
    return(
      <div className='RestaurantMenu'>
        <DishList/>
        <Cart/>
      </div>
    );
  }
}

export default RestaurantMenu;