import React, { Component } from 'react';
import './App.css';
import RestaurantMenu from "./Containers/RestaurantMenu/RestaurantMenu";

class App extends Component {
  render() {
    return (
      <div className="App">
        <RestaurantMenu/>
      </div>
    );
  }
}

export default App;
