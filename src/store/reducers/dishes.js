import * as actionTypes from '../actions/actionTypes';


const initialState = {
  dishes: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SUCCESS_RESPONSE:
      return {...state, dishes: action.dishes};
    default:
      return state;
  }
};


export default reducer;