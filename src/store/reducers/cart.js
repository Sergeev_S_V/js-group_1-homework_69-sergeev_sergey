import * as actionTypes from '../actions/actionTypes';

const initialState = {
  selectedDishes: {},
  totalPrice: 150,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CREATE:
      return {...state,
        selectedDishes: {...state.selectedDishes,
          [action.dishName]: {amount: 1, price: action.cost}},
        totalPrice: state.totalPrice + action.cost};
    case actionTypes.INCREASE:
      return {...state, selectedDishes: {...state.selectedDishes,
          [action.dishName]:
            {amount: state.selectedDishes[action.dishName].amount + 1,
            price: state.selectedDishes[action.dishName].price + action.cost}},
        totalPrice: state.totalPrice + action.cost};
    case actionTypes.REMOVE:
      const newState = {...state, selectedDishes: {...state.selectedDishes},
      totalPrice: state.totalPrice - action.cost};
      delete newState.selectedDishes[action.dishName];
      return newState;
    default:
      return state;
  }
};

export default reducer;