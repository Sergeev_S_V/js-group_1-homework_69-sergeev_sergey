import * as actionTypes from './actionTypes';

export const removed = (dishName, cost) => {
  return {type: actionTypes.REMOVE, dishName, cost};
};

