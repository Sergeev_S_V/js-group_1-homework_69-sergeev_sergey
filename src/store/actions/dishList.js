import * as actionTypes from './actionTypes';
import axios from '../../axios.restorauntMenu';
import {CREATE} from "./actionTypes";
import {INCREASE} from "./actionTypes";

export const getDishes = () => dispatch => {
  dispatch(request());
  axios.get('/dishes.json')
    .then(resp => resp ? dispatch(successResponse(resp.data)) : null);
};

export const request = () => {
  return {type: actionTypes.REQUEST};
};

export const successResponse = dishes => {
  return {type: actionTypes.SUCCESS_RESPONSE, dishes};
};

export const increase = (dishName, cost) => {
  return {type: INCREASE, dishName, cost};
};

export const create = (dishName, cost) => {
  return {type: CREATE, dishName, cost};
};
