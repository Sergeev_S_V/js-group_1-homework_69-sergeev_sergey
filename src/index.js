import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import {Provider} from "react-redux";

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import cart from "./store/reducers/cart";
import dishes from "./store/reducers/dishes";



const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // одно из двух будет использоваться
const rootReducer = combineReducers({
  cart: cart,
  dishes: dishes,
});
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));
const app = (
  <Provider store={store}>
    <App/>
  </Provider>
);


ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
