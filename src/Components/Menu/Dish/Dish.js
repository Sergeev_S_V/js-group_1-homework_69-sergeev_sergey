import React from 'react';
import Button from "../../UI/Button/Button";
import './Dish.css';

const Dish = props => (
  <div className='Dish'>
    <img className='Image' src={props.image} alt=""/>
    <h5>{props.name}</h5>
    <p>Цена: <span>{props.cost}</span></p>
    <Button clicked={props.clicked}>Add to cart</Button>
  </div>
);

export default Dish;