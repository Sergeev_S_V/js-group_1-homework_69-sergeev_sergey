import React, {Component} from 'react';
import './DishList.css';
import {connect} from "react-redux";
import {create, getDishes, increase} from "../../../store/actions/dishList";
import Dish from "../Dish/Dish";

class DishList extends Component {

  componentDidMount() {
    this.props.getDishes();
  };

  clickedOnDish = (dishName, cost) => {
    if (this.props.selectedDishes[dishName]) {
      this.props.increase(dishName, cost);
    } else {
      this.props.create(dishName, cost);
    }
  };

  render() {
    return(
      <div className='DishList'>
        {Object.keys(this.props.dishes).map(dishName => (
          <Dish key={dishName}
                name={this.props.dishes[dishName].name}
                cost={this.props.dishes[dishName].cost}
                image={this.props.dishes[dishName].image}
                clicked={() => this.clickedOnDish(dishName, this.props.dishes[dishName].cost)}
          />
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    dishes: state.dishes.dishes,
    selectedDishes: state.cart.selectedDishes,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getDishes: () => dispatch(getDishes()),
    increase: (dishName, cost) => dispatch(increase(dishName, cost)),
    create: (dishName, cost) => dispatch(create(dishName, cost)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(DishList);

