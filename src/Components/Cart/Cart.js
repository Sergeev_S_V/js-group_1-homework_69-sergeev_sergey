import React, {Component} from 'react';
import './Cart.css';
import {connect} from "react-redux";
import CartItem from "./CartItem/CartItem";
import {removed} from "../../store/actions/cart";
import Modal from "../UI/Modal/Modal";
import ContactData from "../../Containers/ContactData/ContactData";

class Cart extends Component {

  state = {
    purchasing: false
  };

  removedDish = (dishName, cost) => {
    this.props.removed(dishName, cost);
  };

  isPurchasing = () => {
    const sum = Object.keys(this.props.selectedDishes)
      .map(igKey => this.props.selectedDishes[igKey].amount)
      .reduce((sum, el) => sum + el, 0);
    return sum > 0;
  };

  placeOrder = () => {
    this.setState({purchasing: true});
  };

  purchaseCancelHandler = () => {
    this.setState({purchasing: false});
  };

  render() {
    return(
      <div className='Cart'>
        <Modal show={this.state.purchasing}
               closed={this.purchaseCancelHandler}>
          <ContactData dishes={this.props.selectedDishes}
                       price={this.props.totalPrice}/>
        </Modal>
        {Object.keys(this.props.selectedDishes).map(dishName =>
          <CartItem key={dishName}
                    name={dishName}
                    amount={this.props.selectedDishes[dishName].amount}
                    cost={this.props.selectedDishes[dishName].price}
                    clicked={() => this.removedDish(dishName, this.props.selectedDishes[dishName].price)}
          />
        )}
        <p>Доставка: <span className='Delivery'>150 KGS</span></p>
        <p>Итого: <span className='Total-Price'>{this.props.totalPrice} KGS</span></p>
        <button disabled={!this.isPurchasing()}
                onClick={this.placeOrder}>Place order</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedDishes: state.cart.selectedDishes,
    totalPrice: state.cart.totalPrice,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    removed: (dishName, cost) => dispatch(removed(dishName, cost)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);