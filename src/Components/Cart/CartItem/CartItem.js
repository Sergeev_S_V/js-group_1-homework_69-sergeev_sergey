import React from 'react';
import './CartItem.css';

const CartItem = props => {
  return <div className='CartItem'>
    <button className='Remove' onClick={props.clicked}>X</button>
    <p className='Dish-Name'>{props.name}</p>
    <p>Кол-во: <span>х {props.amount}</span></p>
    <p>Цена: <span> {props.cost}</span></p>
  </div>
};

export default CartItem;